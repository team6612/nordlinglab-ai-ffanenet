# FANNEnet #

FANNEnet is a project aim to build convolutional neuron network using firefly algorithm with natural enermy strategy.

### What is this repository for? ###

* Quick summary 
>FFANEprot is a protein function prodictor using FFANEnet  to tuing the convoltuional neural network.
>[FFANEnet](https://github.com/markliou/FFANE) uses the Firefly algorithm with natural enemy strategy for tuning the hyper-paramters for building convolutional neural network. 

* Version
>0.1


### How to use FFANEprot ? ###

#### Description of the essentail files
>FFANEprot have 3 essential python scripts and 1 folder for collecting the results from FFANEnet

* FFANEnet_log
>This is a folder for collecting the results from FFANEnet

* FFANEnet.py
>The essential subroutines including the whole FFANE are contained in this file.

* FFANEnet_main.py
>The main function which give the parameters to FFANEnet.

* tools.py
>The machine learning essential functions, including the data reading, dataset shuffling and early-stoping functions. 

* 2 sample files for training
>toy.swPSSM400 contains the PSSM400 while the toy_labels.labels have the corresponding labels.

#### training and test file format
>The training file should be seperated into 2 files. One of them contains PSSM400 and the other contains the labels. 
>The PSSM400 files use CSV file formats:
>
* 0.608870967742,0.143369175627, ... ,0.283870967742
>
>The labels file is also use the CSV format:
>
* 63,65,90,157,592,608
>
>The labels are made using custermised index for GO terms

### maintainers
yifanliou@gmail.com or mark.liou@nordlinglab.org
