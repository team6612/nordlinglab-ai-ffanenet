#!/usr/bin/python3
#####
## MIT license applied
##             markliou 2017/12/8
#####

import csv
import tensorflow as tf
import numpy as np
import random

import tools


class FFANE():
    '''
    Remember to set fitness function to connect with this dummy fitness.
    Example:
    In main.py:
        def fitness(array):
            result = caculation
            return result

        def main():
            Optimizer = EvolutionAlgorithm.Firefly(10, fitness)
            Optimizer.run()
    '''
    def __init__(self,
                 GeneNo , 
                 fitness, 
                 PopSize = 40, 
                 Iteration = 5000,  
                 alpha = 1.0e-3,  
                 beta = 0.5,  
                 gamma = 1,
                 move_constant = np.exp(1),
                 trap_limit = 5,
                 huntered_rate = 0.01 ):
        self.fitness = fitness # a function
        self.fitness_cnt = 0
        self.trap = 0
        self.PopSize = PopSize
        self.Iteration = Iteration 
        self.GeneNo = GeneNo
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma # absorb coefficient
        self.move_constant = move_constant # inference from PSO
        self.huntered_rate = huntered_rate
        self.trap_limit = trap_limit
        self.Pop = { 'PopSize' : self.PopSize,
                     'GeneNo': self.GeneNo,
                     'CurrentGeneration': 0,
                     'inds' : [ self.gen_ind(GeneNo) for i in range(PopSize)]
                   }

    pass

    def run(self):
        # initializing
        print("initializing the FFANE process ...")
        self.BestInd = self.Pop['inds'][0].copy()
        self.update_fitness()
        self.BestInd = self.Pop['inds'][0].copy() # random assign for initializing
        
        # evolution
        print("Start the FFANE process ...")
        for c_iter in range(1,self.Iteration + 1):
            self.update_alpha(c_iter)
            self.update_gamma(c_iter)
            self.update_position_with_NaturalEnemies(c_iter)
            self.update_fitness()
            self.aristogenics()
            CBestInd = self.find_the_best(self.Pop)
            if self.BestInd['fitness'] > CBestInd['fitness'] :
                self.BestInd = CBestInd.copy()
            pass 
            print('Generation: {}   score:{}'.format(c_iter, self.BestInd['fitness']))
        
        pass
        return self.BestInd
    pass 
    
    def Trans_2_PB(self, Genes):
        #####
        # probability coding inferece from PBPSO algorithm
        # L. Wang, " A Noval Probability Binary Partical Swarm Optimization Algorithm and Its Application,"(2008),pp28, Vol3, No9, Journal of Software
        #####
        
        PBGenes = Genes.copy() 
        
        return ((PBGenes - np.random.random(PBGenes.size).reshape(PBGenes.shape)) > 0).astype(int)
    pass
    
    def find_the_best(self, pop):
        inds = pop['inds'].copy()
        best_ind = inds[np.random.randint(0, self.PopSize-1)].copy()
        for i in inds:
            if i['fitness'] < best_ind['fitness'] :
                best_ind = i.copy()
                
                # enhancing the nature enemy attack if the population do not go far
                if ( (self.BestInd['fitness'] - best_ind['fitness'])/self.BestInd['fitness'] ) > 1e-8:
                    self.trap = -self.trap_limit
                pass
                
            pass
        pass
        # print('out best ind: {}'.format(best_ind['fitness']))
        self.trap += 1
        return best_ind 
    pass

    def update_position(self):
        inds = self.Pop['inds'].copy()

        # making distance matrix
        distance_matrix = []
        fitness_matrix = np.array([])
        matrix_q = np.array( [ inds[j]['Genes'] for j in range(0,len(inds)) ] )

        for i in range(0,len(inds)):
            matrix_s = np.array( [j for j in inds[i]['Genes']] )
            distance_matrix.append( [np.sqrt(np.sum(i)) for i in (matrix_q-matrix_s)**2] ) # distance
            fitness_matrix = np.append(fitness_matrix, inds[i]['fitness']) # fitness (I), using exponential to change the finess toward to small
        pass 

        # fitness, multiplicative inverse
        fitness_matrix = np.power(fitness_matrix, -1)

        # making attractive matrix (beta)
        attractive_matrix = (
                             fitness_matrix * # beta
                             np.exp( ( (self.gamma)*(np.power(distance_matrix,2)) )*-1 )
                            )

        # making delta-x matrix
        delta_x = np.array([ (matrix_q[i] - matrix_q) * 
                              attractive_matrix[i].reshape(-1,1) # beta
                              for i in range(0, len(matrix_q)) ]) # distance * attractive
        delta_x +=  ( (np.random.uniform(0, 1, len(delta_x.flatten())) - 0.5 ) *  # epsilon
                       np.random.uniform(0, self.alpha, len(delta_x.flatten())) # alpha
                    ).reshape(delta_x.shape) # alpha * epsilon
        
        for i in delta_x :
            matrix_q += i
        pass

        np.clip(matrix_q, 0, 1)

        # assign the updated position
        self.Pop['inds'] = [ 
                            {
                            'Genes' : i,
                            'fitness' : np.NAN ,
                            'modify' : True
                            } for i in matrix_q
                           ]
        self.Pop['inds'][np.random.randint(len(matrix_q))] = self.BestInd.copy() # aristogenics

    pass

    def update_position_with_NaturalEnemies(self, NGen):
        #self.gamma = self.update_gamma(NGen)

        inds = self.Pop['inds'].copy()

        # making distance matrix
        distance_matrix = []
        fitness_matrix = np.array([])
        matrix_q = np.array( [ inds[j]['Genes'] for j in range(0,len(inds)) ] )

        for i in range(0,len(inds)):
            matrix_s = np.array( [j for j in inds[i]['Genes']] )
            distance_matrix.append( [np.sqrt(np.sum(i)) for i in (matrix_q-matrix_s)**2] ) # distance
            fitness_matrix = np.append(fitness_matrix, inds[i]['fitness']) # fitness (I), using exponential to change the finess toward to small
        pass 

        # fitness, multiplicative inverse
        fitness_matrix = np.power(fitness_matrix, -1)

        # making attractive matrix (beta)
        attractive_matrix = (
                             fitness_matrix * # beta
                             np.exp( ( (self.gamma)*(np.power(distance_matrix,2)) )*-1 )
                            )

        # making delta-x matrix
        if (self.huntered_rate * np.exp(self.trap - 1)) < np.random.random():
            delta_x = np.array([ 
                                self.move_constant * 
                                (matrix_q[i] - matrix_q) *
                                attractive_matrix[i].reshape(-1,1) # beta
                                for i in range(0, len(matrix_q)) 
                            ]) # distance * attractive
        else:
            delta_x = np.array([ 
                                self.move_constant * 
                                ( matrix_q - matrix_q[i]) * 
                                attractive_matrix[i].reshape(-1,1) # beta
                                for i in range(0, len(matrix_q)) 
                            ]) 
            print("!!")
            self.trap = -self.trap_limit
        pass

        delta_x +=  ( (np.random.uniform(0, 1, len(delta_x.flatten())) - 0.5 ) *  # epsilon
                       np.random.uniform(0, self.alpha, len(delta_x.flatten())) # alpha
                    ).reshape(delta_x.shape) # alpha * epsilon
        
        for i in delta_x :
            matrix_q += i
        pass

        matrix_q = np.clip(matrix_q, 0, 1)

        # assign the updated position
        self.Pop['inds'] = [ 
                            {
                            'Genes' : i,
                            'fitness' : np.NAN ,
                            'modify' : True
                            } for i in matrix_q
                           ]

        

    pass

    def aristogenics(self): # aristogenics using wheel
        inds = self.Pop['inds'].copy()
        fitness_matrix = np.array([])
        for i in range(0,len(inds)):
            fitness_matrix = np.append(fitness_matrix, inds[i]['fitness']) # fitness (I), using exponential to change the finess toward to small
        pass 
        fitness_matrix = (fitness_matrix - fitness_matrix.min()) / (fitness_matrix.max()-fitness_matrix.min())
        fitness_matrix = np.exp(fitness_matrix * -1)
        wheel = np.array([ (fitness_matrix * 100) / np.sum(fitness_matrix) ]).astype(int)

        wheel = np.hstack(np.array([ [i for j in range(0,wheel[0][i])]  for i in range(0,len(wheel[0]))]))
        np.random.shuffle(wheel)
        wheel = wheel.astype(int)

        try:
            self.Pop['inds'][wheel[0]] = self.BestInd.copy() 
        except:
            print("convergenced, get into early stop step ...")
            exit()
    pass

    def update_alpha(self, NGen):
        self.alpha *= np.exp(-(self.Iteration - NGen) / self.Iteration) 
    pass

    def update_gamma(self, NGen):
        self.gamma *= (self.Iteration/(np.exp(NGen)-2.17828))
    pass

    def update_fitness(self):
        for ind in self.Pop['inds']:
            if ind['modify'] == True:
                ind['fitness'] = self.fitness(ind['Genes'])
                self.fitness_cnt += 1
                ind['modify'] = False
            pass 
        pass 
    pass

    def gen_ind(self, GeneNo):
        ind = {
               'Genes' : np.array( [ i for i in np.random.random(GeneNo)] ),
               'fitness' : np.NAN ,
               'modify' : True
              }
        return ind 
    pass    

    def fitness():
        '''
        This section depends on the problem
        '''
        print("PLEASE DEFINE THE FITNESS FUNCTION!!!!")
        exit()
    pass

pass 

class AutoArc2DCNN():
    def __init__(self,
                 SaveModel = True,
                 CleanDefaultGraph = True
                ):
        self.SaveModel = SaveModel
        self.CleanDefaultGraph = CleanDefaultGraph
        pass
    pass
    
    def construct_2dcnn(
                        self,
                        x, # x = tf.placeholder(tf.float32, [batch_no, input_dim[0]*input_dim[1]]) # data entry point
                        struc_param,
                        input_dim, # 2D array
                        input_channel_dim,
                        output_dim
                        ):
        '''
        struc_param : [kernel size, layer number, output channel number ]
        x : data input point. This should be a flat form. 
        input_dim : the basic shape of x 
        input_channel_dim : the channel number of x 
        output_dim : how many labels need to be output
        '''
        
        current_construct_layer_no = 0
        
        kernels, cl_biases, fc_weights, fc_biases = [], [], [], [] # building weights and biases containers
        
        ### construct weights and biases
        # the kernels and biases of 1st layer
        kernels.append([])
        cl_biases.append([])
        for j in range( 0, int(struc_param['Activated_CL_nodes'][0]) ):
            kernels[0].append(tf.Variable(tf.random_normal([struc_param['kernel'][0][j], struc_param['kernel'][0][j], input_channel_dim, 1] ,stddev= struc_param['CL_initial'][0][j]) ) )
            cl_biases[0].append(tf.Variable( tf.zeros([1] )))
        
        # the kernels and biases of 2~last layers
        for i in range(1, int(struc_param['Active_CL']) ): # create convlution layers (2~last layers)
            kernels.append([])
            cl_biases.append([])
            for j in range(0, struc_param['Activated_CL_nodes'][i]):
                kernels[i].append(tf.Variable(tf.random_normal([struc_param['kernel'][i][j], struc_param['kernel'][i][j], struc_param['Activated_CL_nodes'][i-1], 1] ,stddev= struc_param['CL_initial'][i][j]) ) )
                cl_biases[i].append(tf.Variable( tf.zeros([1] )))
        pass

        fc_weights.append(tf.Variable(tf.random_normal([(input_dim[0] * input_dim[1] * struc_param['Activated_CL_nodes'][struc_param['Active_CL']-1] + (400 * input_channel_dim)), struc_param['FC_node'][0] ] ,stddev=struc_param['FC_initial'][0] ) )) # first fully connect layer
        fc_biases.append(tf.Variable( tf.zeros( [ struc_param['FC_node'][0] ] ))) # bias for first fully connect layer
        for i in range(1, int(struc_param['Active_FC']) ): # create fully connect layers
            fc_weights.append(tf.Variable(tf.random_normal([struc_param['FC_node'][i-1], struc_param['FC_node'][i]] ,stddev=struc_param['FC_initial'][i] ) ))
            fc_biases.append(tf.Variable( tf.zeros([struc_param['FC_node'][i]] )))
        pass
        
        # out_weights = tf.Variable(tf.random_normal([nodeno, output_dim]))
        out_weights = tf.Variable(tf.zeros([struc_param['FC_node'][struc_param['Active_FC']-1], output_dim]))
        out_bias = tf.Variable(tf.zeros([output_dim] ))
        
        ### construct the network
        # Reshape input picture
        x = tf.reshape(x, shape=[-1, input_dim[0], input_dim[1], input_channel_dim])
        
        layers = [] # initial the NN
        
        #### building convolution layers
        layers.append(tf.concat( [ self.conv2d(x, kernels[0][j], cl_biases[0][j], struc_param['CL_lambda'][0][j], struc_param['CL_alpha'][0][j]) for j in range(0, struc_param['Activated_CL_nodes'][0]) ], 
                                   axis=3
                                )
                         
                      )
        current_construct_layer_no += 1
        for i in range(1, int(struc_param['Active_CL']) ):
            layers.append(tf.concat( [ self.conv2d(layers[i-1], kernels[i][j], cl_biases[i][j], struc_param['CL_lambda'][i][j], struc_param['CL_alpha'][i][j] ) for j in range(0, struc_param['Activated_CL_nodes'][i]) ], 
                                     axis=3
                                    )
                         
                         )
            current_construct_layer_no += 1
        pass
        

        #### building fully connected layers
        
        # reshaped_feature_mapes = tf.reshape(layers[current_construct_layer_no - 1], [-1, fc_weights[0].get_shape().as_list()[0]])
        reshaped_feature_mapes = tf.reshape(tf.concat([layers[current_construct_layer_no - 1], x], axis = 3), [-1, fc_weights[0].get_shape().as_list()[0]]) # condense the original PSSM into fully connect layer
        
        layers.append(self.fc(reshaped_feature_mapes, fc_weights[0], fc_biases[0], struc_param['FC_lambda'][0], struc_param['FC_alpha'][0]))
        current_construct_layer_no += 1
        
        for i in range(1, int(struc_param['Active_FC']) ):
            layers.append(self.fc(layers[current_construct_layer_no - 1], fc_weights[i], fc_biases[i], struc_param['FC_alpha'][i], struc_param['FC_lambda'][i]))
            current_construct_layer_no += 1
        pass
        
        # output layer
        out = tf.add(tf.matmul(layers[current_construct_layer_no - 1], out_weights), out_bias)
        # out = tf.nn.sigmoid(tf.add(tf.matmul(layers[current_construct_layer_no - 1], out_weights), out_bias))
        
        self.pred = out
        return out
    pass

    def conv2d(self, x, W, b, FFANelu_alpha=1, FFANelu_lambda=1, strides=1):
        x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
        x = tf.nn.bias_add(x, b)
        return self.FFANelu(x, FFANelu_alpha, FFANelu_lambda)# return self.FFANelu(x, 1.6732632423543772848170429916717, 1.0507009873554804934193349852946)
    pass
    
    def fc(self, x, W, b, FFANelu_alpha=1, FFANelu_lambda=1):
        x = tf.add(tf.matmul(x, W), b)
        return self.FFANelu(x, FFANelu_alpha, FFANelu_lambda)# return self.FFANelu(x, 1.6732632423543772848170429916717, 1.0507009873554804934193349852946)
    pass

    def selu(self, x, alpha = 1.6732632423543772848170429916717, scale = 1.0507009873554804934193349852946):
        return scale*tf.where(x>=0.0, x, alpha*tf.nn.elu(x))
    pass
    
    def FFANelu(self, x, alpha = 1, lamb = 1):
        return tf.where(x>=0.0, lamb * x, alpha*tf.nn.elu(x))
    pass

    def TDCNN_run ( # TDCNN means '2D CNN'
            self,
            # x, 
            # y, 
            TrainingBasicGenerator,
            TrainingDataHandler,
            struc_param,
            input_dim,
            input_channel_dim,
            output_dim,
            batch_size,
            
            # Parameters
            learning_rate = 1E-3,
            training_iters = 10000000,
            display_step = 10,
            pos_panalty = 1,
            PQAlpha = 2,
            stop_epoch = -1
            ):
            
        # construct the placeolder for input data
        x = tf.placeholder(tf.float32, [None, input_dim[0]*input_dim[1]])
        y = tf.placeholder(tf.float32, [None, output_dim])
        
        
        pred = self.construct_2dcnn( x, 
                                     struc_param = struc_param, 
                                     input_dim = input_dim, 
                                     input_channel_dim = input_channel_dim, 
                                     output_dim = output_dim)

                               
        # Define loss and optimizer

        cost = tf.reduce_mean(tf.nn.weighted_cross_entropy_with_logits(logits=pred, targets=y, pos_weight=pos_panalty)) 
        # cost = tf.reduce_mean(tf.nn.weighted_cross_entropy_with_logits(logits=tf.nn.sigmoid(pred), targets=y, pos_weight=pos_panalty)) 
        MCC = self.Matthew_correlation_coefficient(y, tf.round(pred))
        # MCC = self.Matthew_correlation_coefficient(y, tf.round(tf.nn.sigmoid(pred)))
        cost += tf.reduce_mean(tf.losses.hinge_loss(logits=pred, labels=y)) 
        # cost += (1 - MCC)
        # optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost) 
        optimizer = tf.contrib.opt.NadamOptimizer(learning_rate=learning_rate).minimize(cost)

        # Evaluate model
        correct_pred = tf.equal(tf.round(pred), y)
        accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
        
        

        # Initializing the variables
        init = tf.global_variables_initializer()

        # Launch the graph
        tfconfig = tf.ConfigProto(gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.45))
        tfconfig.gpu_options.allow_growth = True
        model_saver = tf.train.Saver()
        with tf.Session(config = tfconfig) as sess:
            tf_log_writer = tf.summary.FileWriter("./log", graph = sess.graph) # for tensorboard
            sess.run(init)
            step = 1
            epoch = 1
            
            # initial the stopping criteria
            Tr_feature_batch, Tr_label_batch = TrainingDataHandler.training_batch(batch_size, TrainingBasicGenerator)
            validation_x = np.vstack(Tr_feature_batch)
            validation_y = np.vstack([ TrainingDataHandler.trans2onehot(i, output_dim) for i in Tr_label_batch ])
            c_loss = sess.run(cost, feed_dict={x: validation_x, y: validation_y})
            stop_criteria = tools.PQAlpha( c_loss, alpha = PQAlpha )
            # print("Initial loss :{}  learning rate:{}".format(c_loss, learning_rate))
            
            # Keep training until reach max iterations
            while (stop_criteria.Stop == False) and (not TrainingDataHandler.epoch == stop_epoch):
                Tr_feature_batch, Tr_label_batch = TrainingDataHandler.training_batch(batch_size, TrainingBasicGenerator)
                Tr_x = np.vstack(Tr_feature_batch)
                Tr_y = np.vstack([ TrainingDataHandler.trans2onehot(i, output_dim) for i in Tr_label_batch ])
                
                sess.run(optimizer, feed_dict={x: Tr_x, y: Tr_y})
                self.model_loss = sess.run(cost, feed_dict={x: Tr_x,
                                                            y: Tr_y})
                stop_criteria.update(self.model_loss)
                
                if step % display_step == 0:
                    # Calculate batch loss and accuracy
                    loss, acc, mcc = sess.run([cost, accuracy, MCC], feed_dict={x: Tr_x,
                                                                      y: Tr_y})
                    # print("Sampling times: " + str(step*batch_size) + \
                          # ", Minibatch Loss= " + "{:.6f}".format(loss) + \
                          # ", Training Accuracy= " + "{:.5f}".format(acc) +\
                          # ", MCC = " + "{:.5f}".format(mcc)
                          # )
                          
                step += 1
                
            
            # saving the model
            if self.SaveModel:
                print(model_saver.save(sess, './models/AutoArc'))
            pass

        pass  # end 'with'
        
        if self.CleanDefaultGraph :
            tf.reset_default_graph()
        pass
        
        return self.model_loss
    pass
    
    def Matthew_correlation_coefficient(self, y, _y):
        _y_pos = _y 
        _y_neg = 1 - _y
        
        y_pos = y 
        y_neg = 1 - y 
        
        tp = tf.reduce_sum(y_pos * _y_pos)
        tn = tf.reduce_sum(y_neg * _y_neg)
        
        fp = tf.reduce_sum(y_neg * _y_pos)
        fn = tf.reduce_sum(y_pos * _y_neg)
        
        up   = tf.cast((tp * tn - fp * fn), dtype = tf.float32)
        down = tf.sqrt(tf.cast(((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn)), dtype = tf.float32))
        
        return up/down
    pass
    
pass

 
class Data_handler():
    def __init__(self):
        self.epoch = 0
    pass
    
    def trans2onehot(self, sparse_ind, output_dim):
        onehoty = np.zeros([output_dim])
        
        if type(sparse_ind) == list :
            for i in sparse_ind:
                onehoty[int(i)] = 1
            pass 
        else:
            onehoty[sparse_ind] = 1
        pass
        
        return onehoty
    pass

    def read_dataset(self, filename):
        content_container = []
        csvfile = open(filename)
        csvreader = csv.reader(csvfile)
        for content in csvreader:
            content_container.append(content)
        pass
        csvfile.close()
        
        # return np.vstack(content_container)
        return content_container
    pass 

    def training_data_generator(self, Tr_features, Tr_labels):
        assert len(Tr_features) == len(Tr_labels)
        self.epoch = 0
        s_index = [i for i in range(len(Tr_labels))]
        ss_index = s_index[:]
        random.shuffle(ss_index)
        while(1):
            if len(ss_index) == 0:
                self.epoch += 1
                ss_index = s_index[:]
                random.shuffle(ss_index)
                # print("epoch: {} shuffle epoch!".format(self.epoch))
            else :
                pop_index = ss_index.pop()
                yield(Tr_features[pop_index], Tr_labels[pop_index])
            pass
        pass
    pass

    def training_data_generator_with_noise(self, Tr_features, Tr_labels, noise_rate):
    ##
    ## mode collapse would be solved using some noise
    ##
        assert len(Tr_features) == len(Tr_labels)
        epoch = 0
        s_index = [i for i in range(len(Tr_labels))]
        ss_index = s_index[:]
        random.shuffle(ss_index)
        while(1):
            if len(ss_index) == 0:
                epoch += 1
                ss_index = s_index[:]
                random.shuffle(ss_index)
                # print("epoch: {} shuffle epoch!".format(epoch))
            else :
                pop_index = ss_index.pop()
                if random.random() < noise_rate :
                    pop_index_i = random.choice(s_index)
                    pop_index_j = random.choice(s_index)
                    yield(Tr_features[pop_index_i], Tr_labels[pop_index_j])
                pass 
                yield(Tr_features[pop_index], Tr_labels[pop_index])
            pass
        pass
    pass

    def training_data_generator_2C(self, Tr_features1, Tr_features2, Tr_labels):
        assert len(Tr_features1) == len(Tr_labels)
        s_index = [i for i in range(len(Tr_labels))]
        ss_index = s_index[:]
        random.shuffle(ss_index)
        while(1):
            if len(ss_index) == 0:
                ss_index = s_index[:]
                random.shuffle(ss_index)
                #print("shuffle epoch!")
            else :
                pop_index = ss_index.pop()
                yield(Tr_features1[pop_index], Tr_features2[pop_index], Tr_labels[pop_index])
            pass
        pass
    pass

    def training_batch(self, batch_size, basic_generator):
        Tr_feature_batch, Tr_label_batch = [], []
        for i in range(0, batch_size):
            T_feature_batch, T_label_batch = next(basic_generator)
            Tr_feature_batch.append(T_feature_batch)
            Tr_label_batch.append(T_label_batch)
        pass 
        return Tr_feature_batch, Tr_label_batch
    pass 
pass


class sample_fitness():
    def __init__(self):
        pass

    def Rastrigin(self, solution):
        '''
        f(0,0) = 0 (minimum)
        search domain : -5.12~5.12
        '''
        A = 10
        return A * len(solution) + np.sum([ i ** 2 - A*np.cos(2*np.pi*i) for i in solution])

pass




